#include <iostream>
#include <fstream>

//Base class
class Item
{
public:
	std::string productID;
	std::string name;
	int quantity;
};

//Derived class CD
class CD : public Item
{
public:
	std::string artist;
	std::string songs[100];

	//function to sell CDs
	int sell()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to sell\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to sell\n";
		std::fstream MyReadFile("CDs.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " sold." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " removed from the CDs stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}

	//function for restocking CDs
	int restock()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to RESTOCK\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to RESTOCK\n";
		std::fstream MyReadFile("CDs.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " restocked." << std::endl;
					myfile.open("CDs.txt", std::ios_base::app);
					myfile << "1 copy of ProductID: " << productID << " added to the CDs stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}
};

//Derived class DVD
class DVD : public Item
{
public:
	std::string artist;
	std::string videos[100];

	//function to sell DVDs
	int sell()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to RESTOCK\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to RESTOCK\n";
		std::fstream MyReadFile("DVDs.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " sold." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " removed from the DVDs stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}

	//function for restocking DVDs
	int restock()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to RESTOCK\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to RESTOCK\n";
		std::fstream MyReadFile("DVDs.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " restocked." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " added to the DVDs stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}
};

//derived class Book
class Book : public Item
{
public:
	std::string author;

	//function for selling Books
	int sell()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to sell\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to sell\n";
		std::fstream MyReadFile("Books.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " sold." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " removed from the Books stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}

	//function for restocking Books
	int restock()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to RESTOCK\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to RESTOCK\n";
		std::fstream MyReadFile("Books.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " restocked." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " added to the Books stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}
};

//derived class Magazine
class Magazine : public Item
{
public:
	std::string author;

	//function for selling magazines
	int sell()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to sell\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to sell\n";
		std::fstream MyReadFile("Magazines.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " sold." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " removed from the Magazines stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}

	//function for restocking magazines
	int restock()
	{
		std::string myText;
		std::cout << "Enter the ProductID of this product that you wish to RESTOCK\n";
		std::cin >> productID;
		std::cout << "Enter the quantity of this product that you wish to RESTOCK\n";
		std::fstream MyReadFile("Magazines.txt");
		bool found = false;
		int i = 0, lineNumber = -1;
		while (getline(MyReadFile, myText) && found == false)
		{
			std::ofstream myfile;
			lineNumber++;
			i = 0;
			while (found == false && i < 100)
			{
				if (myText[i] == productID[2])
				{
					found = true;
					std::cout << quantity << " copy of ProductID: " << productID << " restocked." << std::endl;
					myfile << "1 copy of ProductID: " << productID << " added to the Magazines stock." << std::endl;
					myfile.close();
				}
				else
				{
					i++;
				}
			}
		}
		MyReadFile.close();
		return lineNumber;
	}
};

int main()
{

	//initialization of variables

	//CD
	std::string CDName[100];
	std::string CDPID[100];
	int CDQuantity[100];
	std::string CDArtist[100];
	CD CD1;

	//DVD
	std::string DVDName[100];
	std::string DVDPID[100];
	int DVDQuantity[100];
	std::string DVDArtist[100];
	DVD DVD1;

	//Books
	std::string BookName[100];
	std::string BookPID[100];
	int BookQuantity[100];
	std::string BookAuthor[100];
	Book Book1;

	//Magazines
	std::string MagazineName[100];
	std::string MagazinePID[100];
	int MagazineQuantity[100];
	std::string MagazineAuthor[100];
	Magazine Magazine1;

	int CDamount, DVDamount, Booksamount, Magazinesamount, songsAmount = 0, videosAmount = 0, choice;
	int productAdd;
	int productSell;
	int productRestock;
	bool exit = false;
	std::string productID;
	std::ofstream myfile;
	bool proceed = false;

	//Loop to allow the user to add/sell/restock as many items as they want. Loop ends when user wants to update stock and save updated data to text files
	while (!exit)
	{
		//Menu to help user to interact with the program
		std::cout << "choose what you would like to do\n";
		std::cout << "[1] SELL ITEMS\n";
		std::cout << "[2] RESTOCK ITEMS\n";
		std::cout << "[3] ADD ITEMS\n";
		std::cout << "[4] UPDATE STOCK QUANTITY\n";
		std::cout << "[5] GENERATE SALES REPORT\n";
		std::cout << "[6] EXIT THE PROGRAM\n";

		int userInput;
		std::cin >> userInput;

		//case statements to respond to user request. User input is also validated for each of the above mentioned functionalities
		switch (userInput)
		{
		//implement selling items functionality
		case 1:
			std::cout << "Enter the Name of the product you would like to SELL\n";
			std::cout << "[1] CD\n";
			std::cout << "[2] DVD\n";
			std::cout << "[3] Books\n";
			std::cout << "[4] Magazine\n";
			std::cin >> productSell;
			switch (productSell)
			{
			case 1:
			{
				int lineNumber = CD1.sell();
				std::ofstream myFile;
				myFile.open("CDs.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] << " Artist: " << CDArtist[i] << " Songs:";
						for (int i = 0; i <= songsAmount; i++)
						{
							myFile << " [" << i + 1 << "] " << CD1.songs[i];
						}
						myFile << std::endl;
					}
					else
					{
						myFile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] - 1 << " Artist: " << CDArtist[i] << " Songs:";
						for (int i = 0; i <= songsAmount; i++)
						{
							myFile << " [" << i + 1 << "] " << CD1.songs[i];
						}
						myFile << std::endl;
					}
				}
				myFile.close();
			}

			break;
			case 2:
			{
				int lineNumber = DVD1.sell();
				std::ofstream myFile;
				myFile.open("DVDs.txt", std::ofstream::trunc);
				for (int i = 0; i < DVDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] << " Artist: " << DVDArtist[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] - 1 << " Artist: " << DVDArtist[i] << std::endl;
					}
				}
				myFile.close();
			}

			break;
			case 3:
			{
				int lineNumber = Book1.sell();
				std::ofstream myFile;
				myFile.open("Books.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] << " Author: " << BookAuthor[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] - 1 << " Author: " << BookAuthor[i] << std::endl;
					}
				}
				myFile.close();
			}

			break;
			case 4:
			{
				int lineNumber = Magazine1.sell();
				std::ofstream myFile;
				myFile.open("Magazines.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] << " Author: " << MagazineAuthor[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] - 1 << " Author: " << MagazineAuthor[i] << std::endl;
					}
				}
				myFile.close();
			}
			break;
			default:
				std::cout << "Please enter a valid choice \n";
			}

			break;
		case 2:
			//implement Restocking items functionality
			std::cout << "Enter the Name of the product you would like to RESTOCK\n";
			std::cout << "[1] CD\n";
			std::cout << "[2] DVD\n";
			std::cout << "[3] Books\n";
			std::cout << "[4] Magazine\n";
			std::cin >> productRestock;
			switch (productRestock)
			{
			case 1:
			{
				int lineNumber = CD1.restock();
				std::ofstream myFile;
				myFile.open("CDs.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] << " Artist: " << CDArtist[i] << " Songs:";
						for (int i = 0; i <= songsAmount; i++)
						{
							myFile << " [" << i + 1 << "] " << CD1.songs[i];
						}
						myFile << std::endl;
					}
					else
					{
						myFile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] + 1 << " Artist: " << CDArtist[i] << " Songs:";
						for (int i = 0; i <= songsAmount; i++)
						{
							myFile << " [" << i + 1 << "] " << CD1.songs[i];
						}
						myFile << std::endl;
					}
				}
				myFile.close();
			}
			break;

			case 2:
			{
				int lineNumber = DVD1.restock();
				std::ofstream myFile;
				myFile.open("DVDs.txt", std::ofstream::trunc);
				for (int i = 0; i < DVDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] << " Artist: " << DVDArtist[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] + 1 << " Artist: " << DVDArtist[i] << std::endl;
					}
				}
				myFile.close();
			}
			break;
			case 3:
			{
				int lineNumber = Book1.restock();
				std::ofstream myFile;
				myFile.open("Books.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] << " Author: " << BookAuthor[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] + 1 << " Author: " << BookAuthor[i] << std::endl;
					}
				}
				myFile.close();
			}
			break;
			case 4:
			{
				int lineNumber = Magazine1.restock();
				std::ofstream myFile;
				myFile.open("Magazines.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] << " Author: " << MagazineAuthor[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] + 1 << " Author: " << MagazineAuthor[i] << std::endl;
					}
				}
				myFile.close();
			}
			break;

			default:
				std::cout << "Please enter a valid choice \n";
			}
			break;
		case 3:
			//implement adding items to the stock functionality
			std::cout << "Enter the Name of the product you would like to ADD\n";
			std::cout << "[1] CD\n";
			std::cout << "[2] DVD\n";
			std::cout << "[3] Books\n";
			std::cout << "[4] Magazine\n";

			std::cin >> productAdd;
			switch (productAdd)
			{
			case 1:
				std::cout << "Enter the amount of different CDs that you wish to add\n";
				std::cin >> CDamount;
				for (int i = 0; i < CDamount; i++)
				{
					std::cout << "Enter Name of the CD\n";
					std::cin >> CD1.name;
					std::cout << "Enter ProductID of the CD\n";
					std::cin >> CD1.productID;
					std::cout << "Enter the name of the Artist of the CD\n";
					std::cin >> CD1.artist;
					std::cout << "Enter the quantity of this particular CD that you wish to add to the stock\n";
					std::cin >> CD1.quantity;
					proceed = false;

					//allow user to add songs to the CD
					while (proceed == false)
					{
						std::cout << "Enter the name of the song\n";
						std::cin >> CD1.songs[songsAmount];
						std::cout << "[1] Add more songs [2] continue\n";
						std::cin >> choice;
						if (choice == 1)
						{
							proceed = false;
							songsAmount++;
						}
						else if (choice == 2)
						{
							proceed = true;
						}
					}

					//save data to arrays
					CDName[i] = CD1.name;
					CDPID[i] = CD1.productID;
					CDQuantity[i] = CD1.quantity;
					CDArtist[i] = CD1.artist;
				}
				//copy data from array to file
				myfile.open("CDs.txt", std::ios_base::app);
				for (int i = 0; i < CDamount; i++)
				{
					myfile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] << " Artist: " << CDArtist[i] << " Songs:";
					for (int i = 0; i <= songsAmount; i++)
					{
						myfile << " [" << i + 1 << "] " << CD1.songs[i];
					}
					myfile << std::endl;
				}
				myfile.close();

				break;
			case 2:
				std::cout << "Enter the amount of different DVDs that you wish to add\n";
				std::cin >> DVDamount;
				for (int i = 0; i < DVDamount; i++)
				{
					std::cout << "Enter Name of the DVD\n";
					std::cin >> DVD1.name;
					std::cout << "Enter ProductID of the DVD\n";
					std::cin >> DVD1.productID;
					std::cout << "Enter the name of the Author of the DVD\n";
					std::cin >> DVD1.artist;
					std::cout << "Enter the quantity of this particular DVD that you wish to add to the stock\n";
					std::cin >> DVD1.quantity;
					proceed = false;

					//allow user to add videos to the DVD
					while (proceed == false)
					{
						std::cout << "Enter the name of the video\n";
						std::cin >> DVD1.videos[videosAmount];
						std::cout << "[1] Add more videos [2] continue\n";
						std::cin >> choice;
						if (choice == 1)
						{
							proceed = false;
							videosAmount++;
						}
						else if (choice == 2)
						{
							proceed = true;
						}
					}

					//save data to arrays
					DVDName[i] = DVD1.name;
					DVDPID[i] = DVD1.productID;
					DVDQuantity[i] = DVD1.quantity;
					DVDArtist[i] = DVD1.artist;
				}
				//copying data from array to file
				myfile.open("DVDs.txt", std::ios_base::app);
				for (int i = 0; i < DVDamount; i++)
				{
					myfile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] << " Artist: " << DVDArtist[i] << std::endl;
					for (int i = 0; i <= videosAmount; i++)
					{
						myfile << " [" << i + 1 << "] " << DVD1.videos[i];
					}
					myfile << std::endl;
				}
				myfile.close();

				break;
			case 3:
				std::cout << "Enter the amount of different Books that you wish to add\n";
				std::cin >> Booksamount;
				for (int i = 0; i < Booksamount; i++)
				{
					std::cout << "Enter Name of the Book\n";
					std::cin >> Book1.name;
					std::cout << "Enter ProductID of the Book\n";
					std::cin >> Book1.productID;
					std::cout << "Enter the name of the Author of the Book\n";
					std::cin >> Book1.author;
					std::cout << "Enter the quantity of this particular Book that you wish to add to the stock\n";
					std::cin >> Book1.quantity;

					//save data to arrays
					BookName[i] = Book1.name;
					BookPID[i] = Book1.productID;
					BookQuantity[i] = Book1.quantity;
					BookAuthor[i] = Book1.author;
				}
				//copy data from array to file
				myfile.open("Books.txt", std::ios_base::app);
				for (int i = 0; i < Booksamount; i++)
				{
					myfile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] << " Artist: " << BookAuthor[i] << std::endl;
				}
				myfile.close();

				break;

			case 4:
				std::cout << "Enter the amount of different Magazines that you wish to add\n";
				std::cin >> Magazinesamount;
				for (int i = 0; i < Magazinesamount; i++)
				{
					std::cout << "Enter Name of the Magazine\n";
					std::cin >> Magazine1.name;
					std::cout << "Enter ProductID of the Magazine\n";
					std::cin >> Magazine1.productID;
					std::cout << "Enter the name of the Author of the Magazine\n";
					std::cin >> Magazine1.author;
					std::cout << "Enter the quantity of this particular Magazine that you wish to add to the stock\n";
					std::cin >> Magazine1.quantity;

					//save data to arrays
					MagazineName[i] = Magazine1.name;
					MagazinePID[i] = Magazine1.productID;
					MagazineQuantity[i] = Magazine1.quantity;
					MagazineAuthor[i] = Magazine1.author;
				}
				//copy data from array to file
				myfile.open("Magazines.txt", std::ios_base::app);
				for (int i = 0; i < Magazinesamount; i++)
				{
					myfile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] << " Artist: " << MagazineAuthor[i] << std::endl;
				}
				myfile.close();

				break;
			default:
				std::cout << "Please enter a valid choice \n";
			}
			break;
		case 4:
		{
			int productUpdate;
			//implement updating stock quantity
			std::cout << "Enter the Name of the product whos stock quantity you would like to UPDATE\n";
			std::cout << "[1] CD\n";
			std::cout << "[2] DVD\n";
			std::cout << "[3] Books\n";
			std::cout << "[4] Magazine\n";

			std::cin >> productUpdate;
			switch (productUpdate)
			{
			case 1:
			{
				int quantity, lineNumber = -1;
				std::string myText;
				std::cout << "Enter the ProductID of this product that you wish to update\n";
				std::cin >> productID;
				std::cout << "Enter the quantity of this product that you wish to Add/remove\n";
				std::cin >> quantity;
				std::fstream MyReadFile("CDs.txt");
				bool found = false;
				int i = 0;
				while (getline(MyReadFile, myText) && found == false)
				{
					lineNumber++;
					i = 0;
					while (found == false && i < 100)
					{
						if (myText[i] == productID[2])
						{
							found = true;
							std::cout << lineNumber;
						}
						else
						{
							i++;
						}
					}
				}
				MyReadFile.close();
				std::ofstream myFile;
				std::ofstream myFile2;
				myFile.open("CDs.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] << " Artist: " << CDArtist[i] << " Songs:";
						for (int i = 0; i <= songsAmount; i++)
						{
							myFile << " [" << i + 1 << "] " << CD1.songs[i];
						}
						myFile << std::endl;
					}
					else
					{
						myFile << "Name: " << CDName[i] << " ProductID: " << CDPID[i] << " Amount of CDs: " << CDQuantity[i] + quantity << " Artist: " << CDArtist[i] << " Songs:";
						for (int i = 0; i <= songsAmount; i++)
						{
							myFile << " [" << i + 1 << "] " << CD1.songs[i];
						}
						myFile << std::endl;
						myFile2.open("CDs.txt", std::ofstream::app);
						myFile2 << quantity << " copies of " << CDPID[i] << " added to the CDs stock." << std::endl;
						myFile2.close();
					}
				}
				myFile.close();
			}
			break;
			case 2:
			{
				int quantity, lineNumber = -1;
				std::string myText;
				std::cout << "Enter the ProductID of this product that you wish to update\n";
				std::cin >> productID;
				std::cout << "Enter the quantity of this product that you wish to Add/remove\n";
				std::cin >> quantity;
				std::fstream MyReadFile("DVDs.txt");
				bool found = false;
				int i = 0;
				while (getline(MyReadFile, myText) && found == false)
				{
					lineNumber++;
					i = 0;
					while (found == false && i < 100)
					{
						if (myText[i] == productID[2])
						{
							found = true;
							std::cout << lineNumber;
						}
						else
						{
							i++;
						}
					}
				}
				MyReadFile.close();
				std::ofstream myFile;
				std::ofstream myFile2;
				myFile.open("DVDs.txt", std::ofstream::trunc);
				for (int i = 0; i < DVDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] << " Artist: " << DVDArtist[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << DVDName[i] << " ProductID: " << DVDPID[i] << " Amount of DVDs: " << DVDQuantity[i] + quantity << " Artist: " << DVDArtist[i] << std::endl;
						myFile2.open("CDs.txt", std::ofstream::app);
						myFile2 << quantity << " copies of " << DVDPID[i] << " added to the DVDs stock." << std::endl;
						myFile2.close();
					}
				}
				myFile.close();
			}

			break;
			case 3:
			{
				int quantity, lineNumber = -1;
				std::string myText;
				std::cout << "Enter the ProductID of this product that you wish to update\n";
				std::cin >> productID;
				std::cout << "Enter the quantity of this product that you wish to Add/remove\n";
				std::cin >> quantity;
				std::fstream MyReadFile("Books.txt");
				bool found = false;
				int i = 0;
				while (getline(MyReadFile, myText) && found == false)
				{
					lineNumber++;
					i = 0;
					while (found == false && i < 100)
					{
						if (myText[i] == productID[2])
						{
							found = true;
							std::cout << lineNumber;
						}
						else
						{
							i++;
						}
					}
				}
				MyReadFile.close();
				std::ofstream myFile;
				std::ofstream myFile2;
				myFile.open("Books.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] << " Author: " << BookAuthor[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << BookName[i] << " ProductID: " << BookPID[i] << " Amount of Books: " << BookQuantity[i] + quantity << " Author: " << BookAuthor[i] << std::endl;
						myFile2.open("CDs.txt", std::ofstream::app);
						myFile2 << quantity << " copies of " << BookPID[i] << " added to the Books stock." << std::endl;
						myFile2.close();
					}
				}
				myFile.close();
			}
			break;
			case 4:
			{
				int quantity, lineNumber = -1;
				std::string myText;
				std::cout << "Enter the ProductID of this product that you wish to update\n";
				std::cin >> productID;
				std::cout << "Enter the quantity of this product that you wish to Add/remove\n";
				std::cin >> quantity;
				std::fstream MyReadFile("Magazines.txt");
				bool found = false;
				int i = 0;
				while (getline(MyReadFile, myText) && found == false)
				{
					lineNumber++;
					i = 0;
					while (found == false && i < 100)
					{
						if (myText[i] == productID[2])
						{
							found = true;
							std::cout << lineNumber;
						}
						else
						{
							i++;
						}
					}
				}
				MyReadFile.close();
				std::ofstream myFile;
				std::ofstream myFile2;
				myFile.open("Magazines.txt", std::ofstream::trunc);
				for (int i = 0; i < CDamount; i++)
				{
					if (i != lineNumber)
					{
						myFile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] << " Author: " << MagazineAuthor[i] << std::endl;
					}
					else
					{
						myFile << "Name: " << MagazineName[i] << " ProductID: " << MagazinePID[i] << " Amount of Magazines: " << MagazineQuantity[i] + quantity << " Author: " << MagazineAuthor[i] << std::endl;
						myFile2.open("CDs.txt", std::ofstream::app);
						myFile2 << quantity << " copies of " << MagazinePID[i] << " added to the Magazines stock." << std::endl;
						myFile2.close();
					}
				}
				myFile.close();
			}
			break;
			default:
				std::cout << "Please enter a valid choice\n";
			}
			break;
		}

		break;
		case 5:
		{
			// Create a text string, which is used to output the text file
			std::string myText;
			//counter variable
			int i;
			std::ifstream MyReadFile("SalesReport.txt");
			// Use a while loop together with the getline() function to read the file line by line
			while (getline(MyReadFile, myText))
			{
				i++;
				// Output the text from the file
				std::cout << "[" << i << "] " << myText << std::endl;
			}
			// Close the file
			MyReadFile.close();
		}
		break;
		case 6:
			exit = true;
			break;
		default:
			std::cout << "Please enter a valid choice\n";
		}
	}
}
